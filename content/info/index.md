---
category: post
title: ""
date: 2022-01-20T10:52:59+08:00
description: ""
draft: false
sitemapExclude: false
---
<div id="pagecontent border">
			<div class="pagetitle padding-pagetitle">
			</div>
			<img src="/images/logo.png" width="240px" class="style-exclude"style="margin-top:2%;">	
		</div>
		<br>
			<div style="text-align:center;">	
		</div>
		<br>
		<button type="button" class="collapsible">Social Media Channels</button>
			<div class="content">
				<table style="margin-left:auto; margin-right:auto;" class="no-table-border">
					<tr><th><img src="/images/fb.png"></th><td><a href="https://facebook.com/dplayzgames06" target="_parent">/dplayzgames06</a></td></tr>
					<tr><th><img src="/images/twch.png"></th><td><a href="https://twitch.tv/dplayzgames06" target="_parent">/dplayzgames06</a></td></tr>
					<tr><th><img src="/images/yt.png"></th><td><a href="https://youtube.com/channel/UCNTjCvAvlLzmEKIZQ5BpoGQ" target="_parent">/dplayz Games</a></td>
					<tr><th><img src="/images/yt.png"></th><td><a href="https://youtube.com/channel/UCRYtnbYg1N9AKS7LQ88N5Qg" target="_parent">dplayz Games VOD</a></td>
				</table>
			</div>
		<button type="button" class="collapsible">Other Accounts</button>
			<div class="content">
  				<table style="margin-left:auto; margin-right:auto;" class="no-table-border">
					<tr><th><img src="/images/twt.png"></th><td><a href="https://twitter.com/dplayzgames06" target="_parent">/dplayzgames06</a></td></tr>
					<tr><th><img src="/images/tktk.png"></th><td><a href="https://tiktok.com/dplayzgames06" target="_parent">/dplayzgames06</a></td></tr>
					<tr><th><img src="/images/ig.png"></th><td><a href="https://instagram.com/dplayzgames06" target="_parent">/dplayzgames06</a></td>
					<tr><th><img src="/images/gh.png"></th><td><a href="https://github.com/dplayz" target="_parent">/dplayz</a></td>
					<tr><th><img src="/images/glb.png"></th><td><a href="https://gitlab.com/dplayz" target="_parent">/dplayz</a></td>
				</table>
			</div>
		<button type="button" class="collapsible">Play With Me!</button>
			<div class="content">
				<table style="margin-left:auto; margin-right:auto;" class="no-table-border">
					<tr><th><img src="/images/mcbe.png"></th><td>dpG06#4539</td></tr>
					<tr><th><img src="/images/osu.png"></th><td><a href="https://osu.ppy.sh/users/19989687">/users/19989687</a></td></tr>
					<tr><th><img src="/images/osudroid.png"></th><td><a href="https://ops.dgsrz.com/profile.php?uid=279686">/profile.php?uid=279686</a></td></tr>
					<tr><th>CODM Garena</th><td>ID: 6927221836920913921</td></tr>
					<tr><th>SSGF/SSJYP</th><td>diipii</td></tr>
					<tr><th><img src="/images/malody.png"></th><td><a href="http://m.mugzone.net/accounts/user/685440">/accounts/user/685440</a></td></tr>
					<tr><th><img src="/images/rblx.png"></th><td><a href="https://www.roblox.com/users/650986696/profile">/users/650986696/profile</a></td></tr>
				</table>
			</div>
		<button type="button" class="collapsible" id="contact-info">Contact Info</button>
				<div class="content">
					<table style="margin-left:auto; margin-right:auto;" class="no-table-border">
						<tr><th>Mail</th><td><a href="mailto:dp@dplayzgames06.tk">dp@dplayzgames06.tk</a></td></tr>
						<tr><th>Blog Feed</th><td>| <a href="/blog/index.xml">RSS</a> | <a href="http://feeds.feedburner.com/dplayzgamesblog">Feedburner Feed</a></td></tr>
						<tr><th><img src="../images/msgr.png"></th><td><a href="http://m.me/dplayzgames06">/dplayzgames06</a></td></tr>
						<tr><th><img src="../images/dscrd.png"></th><td><a href="https://discord.gg/zj88wBrENV">/zj88wBrENV</a></td></tr>
					</table>
				</div>
				<!--Script for collapsable content. Will be moved into a separate file-->
			<script>
				var coll = document.getElementsByClassName("collapsible");
				var i;
					for (i = 0; i < coll.length; i++) {
					coll[i].addEventListener("click", function() {
					this.classList.toggle("active");
					var content = this.nextElementSibling;
						if (content.style.display === "block") {
							content.style.display = "none";
						} else {
							content.style.display = "block";
						}
					});
				}
			</script>
		<br>
		<br>
		<br>
		<br>
		<br>
		</div>

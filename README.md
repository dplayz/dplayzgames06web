<p align="center" width="20px">
  <img src="https://files.dplayzgames06.tk/web/logo.png" alt="logo" width="25%"/><br>  
  <h1 align="center">dplayz Games Website</h1>
</p>

This is the repository of website dplayz Games uses.  
Based on [Hugo](https://gohugo.io), a static site generator using my own coded theme that have similarities on [World of SteelCraft](https://github.com/worldofsteelcraft/woscweb)


## Dependencies
These are the dependencies and needs of this website.
- [Hugo](https://gohugo.io)
- [Disqus](https://disqus.com)

## Contact Information:
WIP
